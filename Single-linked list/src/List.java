public class List {
    LinkList head;
    LinkList tail;
    int size;

    public void addToFront(String data){
        LinkList elem = new LinkList();
        elem.data = data;

        // Проверка если есть head не null, то первому и последнему присваиваем elem
        if (head == null){
            head = elem;
            tail = elem;
        }
        else{ // иначе сдвигаем и добавляем в начало новый элемент
            elem.next = head;
            //elem.previous = head;
            head = elem;
        }
        size++;

    }
    public void addToBack(String data){
        LinkList elem = new LinkList();
        elem.data = data;

        if (tail == null){
            head = elem;
            tail = elem;
        }
        else{
            tail.next = elem;
            tail = elem;
        }
        size++;
    }

    public void addByIndex(String data, int ind){
        LinkList temp = head;
        LinkList elem = new LinkList();
        elem.data = data;

        for (int i = 0; i < size; i++) {
            if(i == ind){
                elem.next = temp.next;
                temp.next = elem;
            }
            temp = temp.next;
        }
        size++;
    }

    void deleteByIndex(int position)
    {
        // Если лист пустой
        if (head == null)
            return;

        LinkList temp = head;

        // Удаляем первый элемент в списке
        if (position == 0)
        {
            head = temp.next;   // меняем голову списка
            return;
        }

        // Find previous node of the node to be deleted
        //Находим предыдущий элемент, который и удаляем
        for (int i=0; temp!=null && i<position-1; i++)
            temp = temp.next;

        // Если переданный индекс больше
        if (temp == null || temp.next == null)
            return;

        // temp->next это элемент/узел, который нужно удалить
        // Сохраняем узлы на следующие элементы
        LinkList next = temp.next.next;

        temp.next = next;  // Unlink the deleted node from list
    }

    public void removeFromFront(){
        if (!(isEmpty())){
            if(head == null){
                System.out.println("List is empty");
            }
            else{
                head = head.next;
            }
            size--;
        }
        else{
            System.out.println("List is empty");
        }
    }

    public void removeFromBack(){
        if (!isEmpty()){
            if(tail == null){
                System.out.println("List is empty");
            }
            else {
                LinkList temp = head;
                LinkList t = new LinkList();

                while (temp.next != null){
                    t = temp;
                    temp = temp.next;
                }
                tail = t;
                tail.next = null;
            }
            size--;
        }
        else{
            System.out.println("List is empty");
        }
    }

    public void findElement(String data){
        if (!(isEmpty())){
            LinkList temp = head;
            int ind = 1;

            while (temp != null){
                if(temp.data.equals(data)){
                    System.out.println("Position: " + ind + "; " + temp.data);
                    return;
                }

                temp = temp.next;
                ind++;
            }
            System.out.println(data + " isn't the list");
        }
        else{
            System.out.println("List is empty");
        }
    }

    private boolean isEmpty(){
        if (size == 0) return true;

        return false;
    }

    // вывод на экран
    public void printQ(){
        if (!(isEmpty())){
            //Создаем новый экземпляр класса
            LinkList temp = head;


            while(temp != null){
                System.out.println(temp.data);
                temp = temp.next;
            }
        }
        else{
            System.out.println("List is empty");
        }
    }
}
