public class MainClass {
    public static void main(String[] args) {
        List list = new List();

        list.addToFront("Alex");
        list.addToFront("Sam");
        list.addToFront("Jo");
        list.printQ();
        list.addByIndex("Dean", 1);
        System.out.println();
        list.printQ();
        System.out.println();
        list.removeFromFront();
        list.printQ();
        list.findElement("Dean");
        System.out.println();
        list.deleteByIndex(2);
        list.printQ();
    }
}
